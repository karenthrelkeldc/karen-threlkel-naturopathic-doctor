Dr. Karen Threlkel, ND is a naturopathic doctor who provides her patients with a full range of naturopathic medical services, including naturopathic medical assessment, specialty lab testing, bio-identical hormone replacement therapy, detoxification, nutritional supplementation and herbal medicine.

Address: 4801 Wisconsin Ave NW, #5, Washington, DC 20016, USA

Phone: 202-235-2149

Website: https://karenthrelkelnd.com
